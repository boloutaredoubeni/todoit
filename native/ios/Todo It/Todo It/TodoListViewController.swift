//
//  TodoViewController.swift
//  Todo It
//
//  Created by Boloutare Doubeni on 4/9/16.
//  Copyright © 2016 Boloutare Doubeni. All rights reserved.
//

import UIKit

import ChameleonFramework
import XCGLogger
import RealmSwift

class TodoListViewController: UIViewController {
	var tableView: UITableView!
	var viewModel: TodoListViewModel!

	let legalPadYellow = HexColor("#faf2ad")
	let log = XCGLogger.defaultInstance()
	let cellReuseIdentifier = "todo_cell"

	required init(coder aDecoder: NSCoder) {
		fatalError("NSCoding not supported")
	}

	init(_ category: String) {
		super.init(nibName: nil, bundle: nil)
		self.viewModel = TodoListViewModel(category)
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		setupView()
		loadRealmData()
	}
}

extension TodoListViewController: UITableViewDelegate { }

extension TodoListViewController: UITableViewDataSource {
	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return viewModel.todos.count
	}

	func tableView(tableView: UITableView,
		cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView
      .dequeueReusableCellWithIdentifier(cellReuseIdentifier, forIndexPath: indexPath) as! TodoItemViewCell
    cell.backgroundColor = legalPadYellow
    let todo = viewModel.todos[indexPath.row]
    cell.nameLabel.text = todo.name
    if let detail = todo.detail {
      cell.detailLabel.text = detail
    }
    // FIXME: add the other stuff
    return cell
	}
}

extension TodoListViewController {
	private func setupView() {
		tableView = UITableView(frame: view.bounds)
		tableView.dataSource = self
		tableView.delegate = self
		tableView.registerClass(TodoItemViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
		tableView.backgroundColor = legalPadYellow
		view.addSubview(tableView)
    
    navigationItem.rightBarButtonItem = editButtonItem() // FIXME: make this an add button

		title = viewModel.category
		log.debug("The view controller has loaded")
	}

	private func loadRealmData() {
		self.viewModel.loadTodos()
		self.tableView.reloadData()
	}
}
