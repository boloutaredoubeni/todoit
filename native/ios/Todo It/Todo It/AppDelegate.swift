//
//  AppDelegate.swift
//  Todo It
//
//  Created by Boloutare Doubeni on 4/9/16.
//  Copyright © 2016 Boloutare Doubeni. All rights reserved.
//

import UIKit

import XCGLogger
import RealmSwift
import ChameleonFramework

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
	let log = XCGLogger.defaultInstance()

	func application(application: UIApplication,
		didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
			// Override point for customization after application launch.
			// NOTE: make sure these are in these order
			setupLogger()
			setupRealm()
			setupWindow()
			return true
	}

	func applicationWillResignActive(application: UIApplication) { }

	func applicationDidEnterBackground(application: UIApplication) { }

	func applicationWillEnterForeground(application: UIApplication) { }

	func applicationDidBecomeActive(application: UIApplication) { }

	func applicationWillTerminate(application: UIApplication) { }

	private func setupWindow() {
		window = UIWindow(frame: UIScreen.mainScreen().bounds)
		let viewController = CategoryViewController()
		let navigationController = UINavigationController(rootViewController: viewController)
		navigationController.navigationBar.barTintColor = FlatBrownDark()
		window!.rootViewController = navigationController
		window!.makeKeyAndVisible()
	}

	private func setupLogger() {
		log.setup(.Verbose,
			showLogIdentifier: true,
			showFunctionName: true,
			showThreadName: true,
			showLogLevel: true,
			showFileNames: true,
			showLineNumbers: true,
			showDate: true,
			writeToFile: "/debug.log",
			fileLogLevel: .Error)
	}

	private func setupRealm() {
		var config = Realm.Configuration()
		config.path = NSURL
			.fileURLWithPath(config.path!)
			.URLByDeletingLastPathComponent?
			.URLByAppendingPathComponent("com.boloutaredoubeni.todoit.realm")
			.path
		Realm.Configuration.defaultConfiguration = config
		log.debug("Created default realm configuration")
		#if DEBUG
			let realm = try! Realm(configuration: config)
			var fakeTodos = [TodoItem]()

			let swiftTodo = TodoItem()
			swiftTodo.category = "Learning"
			swiftTodo.name = "Learn Swift"
			swiftTodo.completed = false
			swiftTodo.important = true
			fakeTodos.append(swiftTodo)

			let appleTodo = TodoItem()
			appleTodo.category = "Groceries"
			appleTodo.completed = true
			appleTodo.important = false
			appleTodo.detail = "You need apples"
			appleTodo.name = "Apples"
			fakeTodos.append(appleTodo)

			let orangeTodo = TodoItem()
			orangeTodo.category = "Groceries"
			orangeTodo.completed = true
			orangeTodo.important = false
			orangeTodo.detail = "Get orange from the store"
			orangeTodo.name = "Oranges"
			fakeTodos.append(orangeTodo)

			try! realm.write {
				realm.add(fakeTodos, update: true)
			}
			log.debug("Initialized the database")
		#endif
	}
}
