//
//  TodoItemViewModel.swift
//  Todo It
//
//  Created by Boloutare Doubeni on 4/9/16.
//  Copyright © 2016 Boloutare Doubeni. All rights reserved.
//

import RealmSwift
import XCGLogger

class TodoItemViewModel {

	let log = XCGLogger.defaultInstance()
	let category: String
	let name: String
	let detail: String?
	let completed: Bool
	let important: Bool

	init(_ todo: TodoItem) {
		category = todo.category
		name = todo.name
		detail = todo.detail
		completed = todo.completed
		important = todo.important
	}

	func save() {
		let config = Realm.Configuration.defaultConfiguration
		let realm = try! Realm(configuration: config)
		try! realm.write {
			let todo = create()
			realm.add(todo, update: true)
			log.debug("Add new todo: \(todo.description)")
		}
	}

	func delete() {
		let config = Realm.Configuration.defaultConfiguration
		let realm = try! Realm(configuration: config)
		try! realm.write {
			let todo = create()
			log.debug("Delete new todo: \(todo.description)")
			realm.delete(todo)
		}
	}

	private func create() -> TodoItem {
		let todo = TodoItem()
		todo.category = category
		todo.completed = completed
		todo.important = important
		todo.detail = detail
		todo.name = name
		return todo
	}
}
