//
//  TodoItemViewCell.swift
//  Todo It
//
//  Created by Boloutare Doubeni on 4/9/16.
//  Copyright © 2016 Boloutare Doubeni. All rights reserved.
//

import UIKit

import ChameleonFramework

class TodoItemViewCell: UITableViewCell {

  var nameLabel = UILabel()
  var detailLabel = UILabel()
  // TODO: Fav button for importance
  // TODO: Strikethrough for completion

  required init(coder aDecoder: NSCoder) {
    fatalError("This is not implemented")
  }

  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)

    contentView.addSubview(nameLabel)
    contentView.addSubview(detailLabel)
  }

  override func layoutSubviews() {
    super.layoutSubviews()

    contentView.backgroundColor = HexColor("#faf2ad")
    nameLabel.frame = CGRect(x: 5, y: 10, width: 300, height: 30)
    detailLabel.frame = CGRect(x: 5, y: 25, width: 300, height: 30)
    detailLabel.font = detailLabel.font.fontWithSize(detailLabel.font.pointSize - 5)
  }
}
