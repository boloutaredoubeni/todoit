//
//  CategoriesViewModel.swift
//  Todo It
//
//  Created by Boloutare Doubeni on 4/9/16.
//  Copyright © 2016 Boloutare Doubeni. All rights reserved.
//

import RealmSwift
import XCGLogger

class TodoListViewModel {

	let log = XCGLogger.defaultInstance()
	private(set) var category: String
	private(set) var todos = [TodoItem]()

	init(_ category: String) {
		self.category = category
	}

	func loadTodos() {
		let config = Realm.Configuration.defaultConfiguration
		let realm = try! Realm(configuration: config)
		todos.appendContentsOf(realm.objects(TodoItem).filter("category == \"\(category)\""))
		log.debug("Retrieved \(self.todos.count) todos")
	}

	func edit(category: String, commit: Bool = false) {
		// TODO: don't change for every edit, create an editing state and when it is done, commit the changes
		// FIXME: What happens if the operation stops half-way?
		let config = Realm.Configuration.defaultConfiguration
		let realm = try! Realm(configuration: config)
		self.category = category
		log.debug("User changed the category \(category)")
		if commit {
			let todos = realm.objects(TodoItem).filter("category == \"\(self.category)\"")
			for todo in todos {
				try! realm.write {
					todo.category = self.category
				}
			}
			log.debug("The category is now saved in realm as \(category)")
		}
	}

	func delete() {
		let config = Realm.Configuration.defaultConfiguration
		let realm = try! Realm(configuration: config)
		let todos = realm.objects(TodoItem).filter("category == \"\(self.category)\"")
		for todo in todos {
			try! realm.write {
				todo.category = self.category
			}
		}
		log.debug("The category and its todos will be deleted")
	}
}
