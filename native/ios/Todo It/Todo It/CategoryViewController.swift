//
//  ViewController.swift
//  Todo It
//
//  Created by Boloutare Doubeni on 4/9/16.
//  Copyright © 2016 Boloutare Doubeni. All rights reserved.
//

import UIKit

import ChameleonFramework
import XCGLogger
import RealmSwift

class CategoryViewController: UIViewController {

	var tableView: UITableView!
	let legalPadYellow = HexColor("#faf2ad")
	var categories: [String] = []
	let log = XCGLogger.defaultInstance()
	let cellReuseIdentifier = "category_cell"

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		setupView()
		loadRealmData()
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
}

extension CategoryViewController: UITableViewDelegate { }

// MARK: UITableViewDataSource
extension CategoryViewController: UITableViewDataSource {

	func numberOfSectionsInTableView(tableView: UITableView) -> Int {
		return 1
	}

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return categories.count
	}

	func tableView(tableView: UITableView,
		cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView
      .dequeueReusableCellWithIdentifier(cellReuseIdentifier, forIndexPath: indexPath)
    cell.backgroundColor = legalPadYellow
    let category = categories[indexPath.row]
    cell.textLabel?.text = category
    cell.selectionStyle = .None
    return cell
	}

	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		let category = categories[indexPath.row]
		let controller = TodoListViewController(category)
		log.debug("Navigating to \(controller.description)")
		navigationController!.pushViewController(controller, animated: true)
	}
  
  func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    switch editingStyle {
    case .Delete:
      let category = categories[indexPath.row]
      categories.removeAtIndex(indexPath.row)
      let config = Realm.Configuration.defaultConfiguration
      let realm = try! Realm(configuration: config)
      let todos = realm.objects(TodoItem).filter("category == \"\(category)\"")
      try! realm.write {
        realm.delete(todos)
      }
      tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    case .Insert:
      // TODO: all updating and creating
      break
    case .None:
      break
    }
  }

  func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    return true
  }
	// FIXME: When selected, sometimes the cell stays gray
}

extension CategoryViewController {

  private func setupView() {
		tableView = UITableView(frame: view.bounds)
		tableView.dataSource = self
		tableView.delegate = self
		tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
		tableView.backgroundColor = legalPadYellow
		view.addSubview(tableView)

    navigationItem.rightBarButtonItem = editButtonItem() // FIXME: make this an add button
    
		title = "Categories"
		log.debug("The view controller has loaded")
	}

	private func loadRealmData() { {
		let config = Realm.Configuration.defaultConfiguration
		let realm = try! Realm(configuration: config)
		let todos = realm.objects(TodoItem)

		for todo in todos {
      if !self.categories.contains(todo.category) {
        self.categories.append(todo.category)
      }
		}

		self.log.debug("Retrieved \(self.categories.count) categories")
		} ~> { self.tableView.reloadData() }
	}
}
