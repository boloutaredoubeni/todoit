//
//  TodoItem.swift
//  Todo It
//
//  Created by Boloutare Doubeni on 4/9/16.
//  Copyright © 2016 Boloutare Doubeni. All rights reserved.
//

import RealmSwift

class TodoItem: Object {
  dynamic var category = ""
  dynamic var name = ""
  dynamic var detail: String?
  dynamic var completed = false
  dynamic var important = false

  override static func primaryKey() -> String {
    return "name"
  }
}
