package com.boloutaredoubeni.todoit.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Copyright 2016 Boloutare Doubeni
 */
public class TodoItem  extends RealmObject {
  private String category;
  @PrimaryKey private String name;
  private String detail;
  private boolean important = false;
  private boolean completed = false;

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

  public boolean isImportant() {
    return important;
  }

  public void setImportant(boolean important) {
    this.important = important;
  }

  public boolean isCompleted() {
    return completed;
  }

  public void setCompleted(boolean completed) {
    this.completed = completed;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    TodoItem todoItem = (TodoItem) o;

    if (!category.equals(todoItem.category)) return false;
    if (!name.equals(todoItem.name)) return false;
    return detail != null ? detail.equals(todoItem.detail) : todoItem.detail == null;

  }

  @Override
  public int hashCode() {
    int result = category.hashCode();
    result = 31 * result + name.hashCode();
    result = 31 * result + (detail != null ? detail.hashCode() : 0);
    return result;
  }


}
