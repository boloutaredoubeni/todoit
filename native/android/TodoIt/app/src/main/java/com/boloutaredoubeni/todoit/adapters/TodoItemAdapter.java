package com.boloutaredoubeni.todoit.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.boloutaredoubeni.todoit.R;
import com.boloutaredoubeni.todoit.models.TodoItem;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.RealmBaseAdapter;
import io.realm.RealmResults;

/**
 * Copyright 2016 Boloutare Doubeni
 */
public class TodoItemAdapter extends RealmBaseAdapter<TodoItem> implements ListAdapter{

  public TodoItemAdapter(Context context, RealmResults<TodoItem> realmResults, boolean automaticUpdate) {
    super(context, realmResults, automaticUpdate);
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    ViewHolder holder;
    if (convertView == null) {
      convertView = inflater.inflate(R.layout.todo_list_item,
          parent, false);
      holder = new ViewHolder(convertView);
      convertView.setTag(holder);
    } else {
      holder = (ViewHolder) convertView.getTag();
    }
    TodoItem todo = realmResults.get(position);
    holder.name.setText(todo.getName());
    holder.detail.setText(todo.getDetail() != null ? todo.getDetail() : "");
    return convertView;
  }

  static class ViewHolder {
    @Bind(R.id.todo_name) TextView name;
    @Bind(R.id.todo_detail) TextView detail;

    public ViewHolder(View itemView) {
      ButterKnife.bind(this, itemView);
    }
  }
}
