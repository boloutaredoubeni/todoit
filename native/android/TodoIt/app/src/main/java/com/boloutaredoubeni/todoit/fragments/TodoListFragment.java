package com.boloutaredoubeni.todoit.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.boloutaredoubeni.todoit.R;
import com.boloutaredoubeni.todoit.TodoItApplication;
import com.boloutaredoubeni.todoit.adapters.TodoItemAdapter;
import com.boloutaredoubeni.todoit.models.TodoItem;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import timber.log.Timber;

/**
 * A placeholder fragment containing a simple view.
 */
public class TodoListFragment extends Fragment {

  private Realm realm;
  private String category = "";
  private TodoItemAdapter adapter;
  @Bind(R.id.todo_list)
  ListView todoList;

  public TodoListFragment() {
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View root =  inflater.inflate(R.layout.fragment_todo_list, container, false);
    ButterKnife.bind(this, root);
    return root;
  }

  @Override
  public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    try {
      category = getActivity().getIntent().getStringExtra(TodoItApplication.CATEGORY);
    } catch (Exception e) {
      Timber.e("Unable to fetch todos");
    }
    realm = Realm.getDefaultInstance();
    RealmResults<TodoItem> todos = realm.where(TodoItem.class).equalTo("category", category).findAllAsync();
    adapter = new TodoItemAdapter(getActivity(), todos, true);
    todoList.setAdapter(adapter);
    todos.addChangeListener(new RealmChangeListener() {
      @Override
      public void onChange() {
        adapter.notifyDataSetChanged();
      }
    });
  }
}
