package com.boloutaredoubeni.todoit;

import android.app.Application;

import com.boloutaredoubeni.todoit.models.TodoItem;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;

/**
 * Copyright 2016 Boloutare Doubeni
 */
public class TodoItApplication extends Application {

  public static final String CATEGORY = "C4T3G0ry";

  @Override
  public void onCreate() {
    super.onCreate();
    // if (BuildConfig.DEBUG) {
    Timber.plant(new Timber.DebugTree());
    // else { }
    setupRealm();
  }

  private void setupRealm() {
    RealmConfiguration config = new RealmConfiguration.Builder(this)
        .name("com.boloutaredoubeni.todoit.realm")
        .build();
    Realm.setDefaultConfiguration(config);
    if (BuildConfig.DEBUG) {
      Realm realm = Realm.getDefaultInstance();
      realm.executeTransactionAsync(new Realm.Transaction() {
        @Override
        public void execute(Realm realm) {
          TodoItem javaTodo = realm.createObject(TodoItem.class);
          javaTodo.setCategory("Learning");
          javaTodo.setName("Learn Java");
          javaTodo.setCompleted(true);
          javaTodo.setImportant(true);

          TodoItem coffeeTodo = realm.createObject(TodoItem.class);
          coffeeTodo.setCategory("Shopping");
          coffeeTodo.setName("Get a coffee beans");
          coffeeTodo.setDetail("We are out!!!");

          TodoItem sugarTodo = realm.createObject(TodoItem.class);
          sugarTodo.setCategory("Shopping");
          sugarTodo.setName("Get Sugar");
          sugarTodo.setCompleted(true);
        }
      }, new Realm.Transaction.OnSuccess() {
        @Override
        public void onSuccess() {
          Timber.d("The database has been setup! with data");
        }
      }, new Realm.Transaction.OnError() {
        @Override
        public void onError(Throwable error) {
          Timber.e("The database was not able to init data");
        }
      });
    }
  }
}
