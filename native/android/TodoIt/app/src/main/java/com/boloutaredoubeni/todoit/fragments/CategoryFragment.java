package com.boloutaredoubeni.todoit.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.boloutaredoubeni.todoit.R;
import com.boloutaredoubeni.todoit.TodoItApplication;
import com.boloutaredoubeni.todoit.activities.TodoListActivity;
import com.boloutaredoubeni.todoit.models.TodoItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Copyright 2016 Boloutare Doubeni
 */
public class CategoryFragment extends Fragment {

  private Realm realm;
  @Bind(R.id.category_list) ListView listView;
  private List<String> categories;
  private ArrayAdapter<String> adapter;

  public CategoryFragment() {
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View root = inflater.inflate(R.layout.fragment_category, container, false);
    ButterKnife.bind(this, root);
    categories = new ArrayList<>();
    adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, categories);
    listView.setAdapter(adapter);
    return root;
  }

  @Override
  public void onStart() {
    super.onStart();
    realm = Realm.getDefaultInstance();
    RealmResults<TodoItem> todos = realm.where(TodoItem.class).findAll();
    for (TodoItem todo : todos) {
      if (!categories.contains(todo.getCategory())) {
        categories.add(todo.getCategory());
      }
    }
  }

  @Override
  public void onStop() {
    super.onStop();
    realm.close();
  }

  @OnItemClick(R.id.category_list)
  void onItemClick(int position) {
    String category = adapter.getItem(position);
    Intent i = new Intent(getActivity(), TodoListActivity.class);
    i.putExtra(TodoItApplication.CATEGORY, category);
    startActivity(i);
  }
}
